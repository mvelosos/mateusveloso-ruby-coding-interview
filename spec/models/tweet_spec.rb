require 'rails_helper'

RSpec.describe Tweet, type: :model do
  context "validations" do
    it "is vald when body characters are less than 180" do
      body = Faker::GreekPhilosophers.quote
      expect(FactoryBot.build(:tweet, body: body)).to be_valid
    end

    it "valid when body characters are 180" do
      body = ""
      180.times do |n|
        body += "b"
      end

      expect(FactoryBot.build(:tweet, body: body)).to be_valid
    end

    it "is not valid when body characters are more than 180" do
      body = SecureRandom.hex(180)

      expect(FactoryBot.build(:tweet, body: body)).not_to be_valid
    end
  end

  context "#check_existing_tweet" do
    it "is not valid when user created a tweet with same body within 24 hours" do
      body = Faker::GreekPhilosophers.quote
      user = FactoryBot.create(:user)
      FactoryBot.create(:tweet, user: user, body: body)
      tweet2 = FactoryBot.build(:tweet, user: user, body: body)

      expect(tweet2).not_to be_valid
    end
  end
end
