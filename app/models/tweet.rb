class Tweet < ApplicationRecord
  belongs_to :user

  # validates :body, presence: true, length: { maximum: 180 }
  # validate :check_existing_tweet

  def check_existing_tweet
    tweets = Tweet.where(user_id: user_id, body: body)
    tweets = tweets.where('created_at >= ?', Time.current - 24.hours)

    if tweets.present?
      errors.add(:tweet, "You already created a tweet with the same content in the past 24 hours")
    end
  end
end
